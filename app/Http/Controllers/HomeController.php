<?php

namespace App\Http\Controllers;

use App\User;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function tablaUser()
    {
        
        
    
        $productos = User::orderBy('created_at','asc')->get();
      
            
        $arr_esc = array();

        
       

        foreach ( $productos as  $producto) {
                 
           
       $acciones = '<button type="button" class="btn btn-primary btn-circle" onclick="productoEditar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Editar</button> &nbsp; '.
       '<button type="button" class="btn btn-danger  btn-circle "  onclick="productoEliminar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Eliminar</button> &nbsp; ';
      
        
            array_push($arr_esc, array(
                'id'=>$producto->id,
                'name'=>$producto->name,
                'email'=>$producto->email,

            
                'boton'=>$acciones,

                
            ));

        }
      
        return response()->json(['response' => 'success', 'status' => 1, 'data' => ($arr_esc)],200);

    }

    public function delete(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = User::find($request->id);
                     
            $esr->delete();

            DB::commit();

            DB::commit();
           
                return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }
        return response()->json(['response' => 'success', 'status' => 3],200);
    }

}
