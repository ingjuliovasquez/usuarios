$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });                       
    TablaPotables();
    
});


function TablaPotables() {
    $('#tablaPotables').DataTable({
        "pageLength": 25,
        "searching": true,
        dom: 'Bfrtip',
        buttons: [
            'excel'
        ],
        'ajax': {
            "type": "GET",  
            "url" :  "/home/usuarios",
        },
        'columns': [
            {data:"id", className: 'hidden-xs'},
            {data:"name", className: 'text-center'},
            {data:"email", className: 'text-center'},
             {data:"boton" , className: 'text-center'}

        ],
        language: {
			sProcessing:    "Procesando...",
			sLengthMenu:    "Mostrar _MENU_ registros",
			sZeroRecords:   "No se encontraron resultados",
			sEmptyTable:    "Ningun resultado encontrado",
			sInfo:          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			sInfoEmpty:     "Mostrando registros del 0 al 0 de un total de 0 registros",
			sInfoFiltered:  "(filtrado de un total de _MAX_ registros)",
			sInfoPostFix:   "",
			sSearch:        "<span class='fa fa-search'></span> Buscar:",
			sUrl:           "",
			sInfoThousands:  ",",
			sLoadingRecords: "Cargando...",
			oPaginate: {
				sFirst:    "Primero",
				sLast:    "Último",
				sNext:    "Siguiente",
				sPrevious: "Anterior"
			}
		}
    });
}




function productoEliminar(id){




 
    var _token = $('meta[name="csrf-token"]').attr('content')

   console.log("Este es el id"+id);

    Swal.fire({
     title: "Se eliminara  Usuario.",
     text: "",
     type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Aceptar",
    confirmButtonColor: '#b1262b',
    cancelButtonText: "Cancelar",
    cancelButtonColor: '#333333',
}).then((result) => {
    if (result.value) {
        $.ajax({
            type: "POST",
            url: "/home/delete",
            data: {
                'id': id
           
            },
            dataType: "JSON",
            success: function (response) {
                if (response.status == 1) {
                    Swal.showLoading();
                
                 Swal.fire('Usuario Eliminado','Acción realizada con exito','success');
                 $('#myModal').modal('hide'); 
                $('#tablaPotables').DataTable().ajax.reload();
                } else {
                    Swal.fire('Ocurrio un problema, intente nuevamente','warning')
                    console.log($error)
                }
                
            }
        });
     }
 });


}




function productoEditar(id){



$('#editPotables').modal('show');  

// ('#producto').val(name); //mandar valor al input





$('.EditarProducto').on( 'click',  function () {
  
    var _token = $('meta[name="csrf-token"]').attr('content')
    // console.log("Este es el token"+_token);
    var producto = $('#producto').val();
     var rangoActualizado =  $('#categoria_id').val(); 

   console.log("Este es el token"+id);

    Swal.fire({
     title: "Se actualizara producto.",
     text: "",
     type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Aceptar",
    confirmButtonColor: '#b1262b',
    cancelButtonText: "Cancelar",
    cancelButtonColor: '#333333',
}).then((result) => {
    if (result.value) {
        $.ajax({
            type: "POST",
            url: route('edit.usuario'),
            data: {
           
            },
            dataType: "JSON",
            success: function (response) {
                if (response.status == 1) {
                    Swal.showLoading();
                
                 Swal.fire('producto Actualizado','Acción realizada con exito','success');
                 $('#editPotables').modal('hide'); 
                $('#tablaPotables').DataTable().ajax.reload();
                } else {
                    Swal.fire('Ocurrio un problema, intente nuevamente','warning')
                    console.log($error)
                }
                
            }
        });
     }
 });
});

}

